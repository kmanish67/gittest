@Lotus @2 @Lotus_Login @Login
Feature: bank
Scenario: 1 goodpath
Given I navigate to "www.noorbank.com"
And I enter into input field "[NationaID]" the value "(NationalID)"
And I enter into input field "[MobileNO]" the value "(MobileNumber123)"
When I click on "[Login]" button
Then I should not see page title as "(Fruit)"
And I enter into input field "[UID]" the value "{Date Pattern : mm/dd/yyyy}"
Then "NA" should have text as "(Username)"
And I take screenshot

Scenario: 2 goodpath
Given I navigate to "www.noorbank.com"

Scenario: 3 badpath
Given I navigate to "www.noorbank.com"
And I take screenshot

Scenario: 4 badpath
Given I navigate to "www.noorbank.com"

Scenario: 5 badpath
Given I navigate to "www.noorbank.com"
And I enter into input field "[NationaID]" the value "(NationalID)"
And I enter into input field "[MobileNO]" the value "(MobileNumber123)"
When I click on "[Login]" button
Then I should not see page title as "(Fruit)"
And I enter into input field "[UID]" the value "{Date Pattern : mm/dd/yyyy}"
And I take screenshot

Scenario: 6 badpath
Given I navigate to "www.noorbank.com"

Scenario: 7 badpath
Given I navigate to "www.noorbank.com"

Scenario: 8 badpath
Given I navigate to "www.noorbank.com"
And I enter into input field "[NationaID]" the value "(NationalID)"
And I enter into input field "[MobileNO]" the value "(MobileNumber123)"
When I click on "[Login]" button
Then I should not see page title as "(Fruit)"
And I enter into input field "[UID]" the value "{Date Pattern : mm/dd/yyyy}"
And I take screenshot

Scenario: 9 badpath
Given I navigate to "www.noorbank.com"

Scenario: 10 badpath
Given I navigate to "www.noorbank.com"
And I take screenshot

