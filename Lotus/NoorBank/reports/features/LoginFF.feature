@Lotus @2 @Lotus_Login @Login
Feature: bank
Scenario: 1  goodpath UID&PWD
Given I navigate to "https://www.noorbank.com"
Then "Valid" should be present
And I enter into input field "[UserName]" the value "(UID)"
And I take screenshot

Scenario: 2  goodpath NalID&MobNum
Given I navigate to "https://www.noorbank.com"
Then "NA" should be present
And I enter into input field "[UserName]" the value "(UID)"

Scenario: 3  badpath NalID&MobNum
Given I navigate to "https://www.noorbank.com"
Then "NA" should be present
And I enter into input field "[UserName]" the value "(UID)"

Scenario: 4  badpath UID&PWD
Given I navigate to "https://www.noorbank.com"
Then "Valid" should be present
And I enter into input field "[UserName]" the value "(UID)"
And I take screenshot

Scenario: 5  badpath NalID&MobNum
Given I navigate to "https://www.noorbank.com"
Then "NA" should be present
And I enter into input field "[UserName]" the value "(UID)"
Then "This is invalid condition" should be present

Scenario: 6  badpath UID&PWD
Given I navigate to "https://www.noorbank.com"
Then "InValid" should be present
And I enter into input field "[UserName]" the value "(UID)"
And I take screenshot

Scenario: 7  badpath UID&PWD
Given I navigate to "https://www.noorbank.com"
Then "Blank" should be present
And I enter into input field "[UserName]" the value "(UID)"
And I take screenshot

Scenario: 8  badpath NalID&MobNum
Given I navigate to "https://www.noorbank.com"
Then "NA" should be present
And I enter into input field "[UserName]" the value "(UID)"
Then "This is invalid condition" should be present

Scenario: 9  badpath UID&PWD
Given I navigate to "https://www.noorbank.com"
Then "Valid" should be present
And I enter into input field "[UserName]" the value "(UID)"
And I take screenshot

Scenario: 10  badpath NalID&MobNum
Given I navigate to "https://www.noorbank.com"
Then "NA" should be present
And I enter into input field "[UserName]" the value "(UID)"

