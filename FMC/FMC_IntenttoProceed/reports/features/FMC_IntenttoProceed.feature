﻿@FMC @0.1 @FMC_Web @IntenttoProceed
Feature: FMC_IntendtoProceed
Scenario: SCN 1 goodpath
Given I navigate to "(AppURL)"
And I enter into input field "[UserID]" the value "(UserID)"
And I capture screenshot
And I enter into input field "[Password]" the value "(Password)"
And I click on "[Login]" button
And I capture screenshot
And I enter into input field "[LoanNum]" the value "(LoanNum)"
When I click on "[GO]" button
Then "[Status]" should have text as "Application received"
And I select from dropdown "[Action]" the value "Intent To Proceed"
And I capture screenshot
And "(AcknowledgementDate)" should be present
And I click on "[MethodReceived]" link
And I capture screenshot
Then "Email, Fax, Phone, In Person, Form" should be present
And I click on "[SpokeWith]" link
And "[Borrower1]" should contain one value of "[SpokewithBorrower]"
When I check the checkbox "[Borrower1]"
Then "[AcknowledgementDate]" should have partial text as "(AcknowledgementDate)"
And I check the checkbox "[IntenttoProceedReceived]"
And "[IntenttoProceedDate]" should have partial text as "(IntenttoProceedDate)"
And I select from dropdown "[MethodReceived]" the value "(MethodReceived)"
And I select from dropdown "[SpokeWith]" the value "(SpokeWith)"
And "[UserID]" should contain one value of "(UserID)"
And I enter into input field "[Comments]" the value "(Comments)"
And I upload to "[Browse]" the file "(SupportingDocs)"
And I click on "[Submit]" button
And I take screenshot
And "[SuccessReply]" should have partial text as "Customer(s) Acknowledgement already received"
And I click on "[Home]" link
And I enter into input field "[LoanNum]" the value "(LoanNum)"
And I click on "[GO]" button
And I select from dropdown "[Action]" the value "Intent to Proceed"
Then "[Borrower]" checkbox should be checked
And "(AcknowledgementDate)" should be present
And "[IntenttoProceedReceived]" checkbox should be checked
And "(IntenttoProceedReceivedDate)" should be present
And "[MethodReceived]" should have text as "(MethodReceived)"
And "[SpokeWith]" should have text as "(SpokeWith)"
And "[Comments]" should have text as "(Comments)"
And I click on "[SupportingDocs]" link
And I click on "[Open]" link
And I capture screenshot
And I click on "[Home]" link
And I enter into input field "[LoanNum]" the value "(LoanNum)"
And I click on "[GO]" button
And I select from dropdown "[Action]" the value "eConsent/Disclosure history"
And I capture screenshot
And "(Spokewith)" should be present
And "(IntenttoProceedReceivedDate)" should be present
And I click on "[Logout]" button
And I close browser
Given I navigate to "(EDMSurl)"
And I enter into input field "[EDMSUID]" the value "(EDMSUID)"
And I enter into input field "[EDMSPWD]" the value "(EDMSPWD)"
And I click on "[EDMSLogin]" button
And I click on "[DocumentManagement]" link
And I enter into input field "[EDMSLoanNum]" the value "(LoanNum)"
And I click on "[FindLoan]" button
And I click on "[Default]" link
And I click on "[RegulatoryDisclosure]" link
When I check elseif "[IntenttoProceed]" value as "(IntenttoProceed)"
And I click on "[IntenttoProceed]" link
And I capture screenshot
And I click on "[Logoff]" link
And I close browser

Scenario: SCN 2 goodpath
Given I navigate to "(AppURL)"
And I enter into input field "[UserID]" the value "(UserID)"
And I capture screenshot
And I enter into input field "[Password]" the value "(Password)"
And I click on "[Login]" button
And I capture screenshot
And I enter into input field "[LoanNum]" the value "(LoanNum)"
When I click on "[GO]" button
Then "[Status]" should have text as "Application received"
And I select from dropdown "[Action]" the value "EditLoan"
And I capture screenshot
And I click on "[SubmitforDisclosure]" button
And I capture screenshot
And I enter into input field "[email1]" the value "(email1)"
And I enter into input field "[email2]" the value "(email2)"
And I click on "[Submit]" button
And I click on "[Logout]" button
And I close browser
Given I navigate to "(DocutechURL)"
And I enter into input field "[PropertyState]" the value "(PropertyState)"
And I enter into input field "[FirstName]" the value "(FirstName)"
And I enter into input field "[LastName]" the value "(LastName)"
And I click on "[Signin]" button
And I click on "[Accept]" button
And I capture screenshot
And I click on "[Sign]" link
And I capture screenshot
And I click on "[Sign]" link
And I capture screenshot
And I click on "[Sign]" link
And I capture screenshot
And I click on "[Sign]" link
And I capture screenshot
And I click on "[Sign]" link
And I capture screenshot
And I click on "[Sign]" link
And I capture screenshot
And I click on "[Close]" link
And I close browser
Given I navigate to "(AppURL)"
And I enter into input field "[UserID]" the value "(UserID)"
And I capture screenshot
And I enter into input field "[Password]" the value "(Password)"
And I click on "[Login]" button
And I capture screenshot
Then "[Borrower]" checkbox should be checked
And "(AcknowledgementDate)" should be present
And "[IntenttoProceedReceived]" checkbox should be checked
And "(IntenttoProceedReceivedDate)" should be present
And "[MethodReceived]" should have text as "(MethodReceived)"
And "[SpokeWith]" should have text as "(SpokeWith)"
And I capture screenshot
And I click on "[Home]" link
And I enter into input field "[LoanNum]" the value "(LoanNum)"
And I click on "[GO]" button
And I select from dropdown "[Action]" the value "eConsent/Disclosure history"
And I capture screenshot
And "(Spokewith)" should be present
And "(IntenttoProceedReceivedDate)" should be present
And I capture screenshot
And I click on "[Logoff]" button
And I close browser

Scenario: SCN 3 goodpath
Given I navigate to "[LKWURL]"
And I click on "[TestLKW]" button
And I enter into input field "[LKWUID]" the value "(LKWUID)"
And I enter into input field "[LKWPWD]" the value "(LKWPWD)"
When I click on "[LKWSubmit]" button
And I enter into input field "[SelectEnvtocheck]" the value "(SelectEnvtocheck)"
And I click on "[LKWEnter]" button
And I click on "[LKWEnter]" button
And I click on "[RetailMortgageProcessing]" button
And I click on "[RetailProcessingMenu]" button
And I enter into input field "[Search]" the value "(LoanNum)"
And I click on "[15Docdelivary]" button
And I click on "[6Grid]" button
And I click on "[3IntenttoProceed]" button
And I capture screenshot
Then "[LKWLoanNum]" should have text as "(LoanNum)"
And "[BorrowerDate]" should have text as "(BorrowerDate)"
And "[ITPReceivedDate]" should have text as "(ITPReceivedDate)"
And "[BorrowerSpokeWith]" should have text as "(BorrowerSpokeWith)"
And I click on "[Logoff]" link
And I close browser

Scenario: SCN 4 badpath
Given I navigate to "(AppURL)"
And I enter into input field "[UserID]" the value "(UserID)"
And I capture screenshot
And I enter into input field "[Password]" the value "(Password)"
And I click on "[Login]" button
And I capture screenshot
Then "[InvalidUID]" should have partial text as "User authentication failed. Either the user name or password was incorrect."
And I click on "[Logout]" button
And I close browser

Scenario: SCN 5 badpath
Given I navigate to "(AppURL)"
And I enter into input field "[UserID]" the value "(UserID)"
And I capture screenshot
And I enter into input field "[Password]" the value "(Password)"
And I click on "[Login]" button
And I capture screenshot
And I enter into input field "[LoanNum]" the value "(LoanNum)"
When I click on "[GO]" button
Then "[Status]" should have text as "Application received"
And I select from dropdown "[Action]" the value "Intent To Proceed"
And I capture screenshot
And I check the checkbox "[Borrower1]"
Then "[AcknowledgementDate]" should have partial text as "(AcknowledgementDate)"
And I check the checkbox "[IntenttoProceedReceived]"
And "[IntenttoProceedDate]" should have partial text as "(IntenttoProceedDate)"
And I select from dropdown "[MethodReceived]" the value "(MethodReceived)"
And I select from dropdown "[SpokeWith]" the value "(SpokeWith)"
And "[UserID]" should contain one value of "(UserID)"
And I enter into input field "[Comments]" the value "(Comments)"
And I upload to "[Browse]" the file "(SupportingDocs)"
And I click on "[Submit]" button
And I take screenshot
And "[InvalidSuppDoc]" should have partial text as "Error Uploading documents. Unsupported File Type. Must be Tiff or PDF. Aborting."
And I click on "[Logout]" button
And I close browser

Scenario: SCN 6 badpath
Given I navigate to "(AppURL)"
And I enter into input field "[UserID]" the value "(UserID)"
And I capture screenshot
And I enter into input field "[Password]" the value "(Password)"
And I click on "[Login]" button
And I capture screenshot
And I enter into input field "[LoanNum]" the value "(LoanNum)"
When I click on "[GO]" button
Then "[Status]" should have text as "Application received"
And I select from dropdown "[Action]" the value "Intent To Proceed"
And I capture screenshot
And I check the checkbox "[Borrower1]"
Then "[AcknowledgementDate]" should have partial text as "(AcknowledgementDate)"
And I check the checkbox "[IntenttoProceedReceived]"
And "[IntenttoProceedDate]" should have partial text as "(IntenttoProceedDate)"
And I select from dropdown "[MethodReceived]" the value "(MethodReceived)"
And "[UserID]" should contain one value of "(UserID)"
And I enter into input field "[Comments]" the value "(Comments)"
And I upload to "[Browse]" the file "(SupportingDocs)"
And I click on "[Submit]" button
And I take screenshot
And "[InvalidSpokewith]" should have partial text as "Please select Borrower who gave Intent to Proceed"
And I click on "[Logout]" button
And I close browser

Scenario: SCN 7 badpath
Given I navigate to "(AppURL)"
And I enter into input field "[UserID]" the value "(UserID)"
And I capture screenshot
And I enter into input field "[Password]" the value "(Password)"
And I click on "[Login]" button
And I capture screenshot
Then "[InvalidUID]" should have partial text as "User authentication failed. Either the user name or password was incorrect.  This User ID does not exist."
And I click on "[Logout]" button
And I close browser

Scenario: SCN 8 badpath
Given I navigate to "(AppURL)"
And I enter into input field "[UserID]" the value "(UserID)"
And I capture screenshot
And I enter into input field "[Password]" the value "(Password)"
And I click on "[Login]" button
And I capture screenshot
And I enter into input field "[LoanNum]" the value "(LoanNum)"
When I click on "[GO]" button
Then "[Status]" should have text as "Application received"
And I select from dropdown "[Action]" the value "Intent To Proceed"
And I capture screenshot
And I check the checkbox "[Borrower1]"
Then "[AcknowledgementDate]" should have partial text as "(AcknowledgementDate)"
And I check the checkbox "[IntenttoProceedReceived]"
And "[IntenttoProceedDate]" should have partial text as "(IntenttoProceedDate)"
And I select from dropdown "[MethodReceived]" the value "(MethodReceived)"
And I select from dropdown "[SpokeWith]" the value "(SpokeWith)"
And "[UserID]" should contain one value of "(UserID)"
And I enter into input field "[Comments]" the value "(Comments)"
And I click on "[Submit]" button
And I take screenshot
And "[InvalidComments]" should have partial text as "Number of characters in comment field should not exceed 255(6)."
And I click on "[Logout]" button
And I close browser

Scenario: SCN 9 badpath
Given I navigate to "(AppURL)"
And I enter into input field "[UserID]" the value "(UserID)"
And I capture screenshot
And I enter into input field "[Password]" the value "(Password)"
And I click on "[Login]" button
And I capture screenshot
And I enter into input field "[LoanNum]" the value "(LoanNum)"
When I click on "[GO]" button
Then "[Status]" should have text as "Application received"
And I select from dropdown "[Action]" the value "Intent To Proceed"
And I capture screenshot
And I check the checkbox "[Borrower1]"
Then "[AcknowledgementDate]" should have partial text as "(AcknowledgementDate)"
And I select from dropdown "[MethodReceived]" the value "(MethodReceived)"
And I select from dropdown "[SpokeWith]" the value "(SpokeWith)"
And "[UserID]" should contain one value of "(UserID)"
And I enter into input field "[Comments]" the value "(Comments)"
And I upload to "[Browse]" the file "(SupportingDocs)"
And I click on "[Submit]" button
And I take screenshot
And "[InvalidItPRec]" should have partial text as "Please select Intent to Proceed Received checkbox."
And I click on "[Logout]" button
And I close browser

Scenario: SCN 10 badpath
Given I navigate to "(AppURL)"
And I enter into input field "[UserID]" the value "(UserID)"
And I capture screenshot
And I enter into input field "[Password]" the value "(Password)"
And I click on "[Login]" button
And I capture screenshot
And I enter into input field "[LoanNum]" the value "(LoanNum)"
When I click on "[GO]" button
Then "[Status]" should have text as "Application received"
And I select from dropdown "[Action]" the value "Intent To Proceed"
And I capture screenshot
And I check the checkbox "[IntenttoProceedReceived]"
And "[IntenttoProceedDate]" should have partial text as "(IntenttoProceedDate)"
And I select from dropdown "[MethodReceived]" the value "(MethodReceived)"
And I select from dropdown "[SpokeWith]" the value "(SpokeWith)"
And "[UserID]" should contain one value of "(UserID)"
And I enter into input field "[Comments]" the value "(Comments)"
And I upload to "[Browse]" the file "(SupportingDocs)"
And I click on "[Submit]" button
And I take screenshot
And "[UncheckBorrowers]" should have partial text as "Acknowledgement checkbox, against All Borrower must be selected"
And I click on "[Logout]" button
And I close browser

Scenario: SCN 11 badpath
Given I navigate to "(AppURL)"
And I enter into input field "[UserID]" the value "(UserID)"
And I capture screenshot
And I enter into input field "[Password]" the value "(Password)"
And I click on "[Login]" button
And I capture screenshot
And I enter into input field "[LoanNum]" the value "(LoanNum)"
When I click on "[GO]" button
Then "[Status]" should have text as "Application received"
And I select from dropdown "[Action]" the value "Intent To Proceed"
And I capture screenshot
And I check the checkbox "[Borrower1]"
Then "[AcknowledgementDate]" should have partial text as "(AcknowledgementDate)"
And I check the checkbox "[IntenttoProceedReceived]"
And "[IntenttoProceedDate]" should have partial text as "(IntenttoProceedDate)"
And I select from dropdown "[SpokeWith]" the value "(SpokeWith)"
And "[UserID]" should contain one value of "(UserID)"
And I enter into input field "[Comments]" the value "(Comments)"
And I upload to "[Browse]" the file "(SupportingDocs)"
And I click on "[Submit]" button
And I take screenshot
And "[InvalidMtdReceived]" should have partial text as "Please select Method Received"
And I click on "[Logout]" button
And I close browser

