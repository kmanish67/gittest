@FMC @0.1 @FMC_Web @USDNNonNorrowe
Feature: BorrowerInfo
Scenario: 1 This is scenario for testing 
Given I navigate to "[AppURL]"
And I enter into input field "[UID]" the value "(UID)"
And I enter into input field "[PWD]" the value "(PWD)"
When I click on "[Login]" button
And I enter into input field "[LoanNumber]" the value "(LoanNumber)"
When I click on "[GO]" button
And I select from dropdown "[Action]" the value "USDAnonBorrower"
When I click on "[InsertHouseHoldMember]" button
And I enter into input field "[HouseHoldmemberName]" the value "(HouseHoldmemberName)"
And I enter into input field "[SSN]" the value "[SSN]"
And I enter into input field "[StreetAdds]" the value "(StreetAdds)"
And I enter into input field "[City]" the value "(City)"
And I enter into input field "[State]" the value "(State)"
And I enter into input field "[Zip]" the value "(Zip)"
And I enter into input field "[eMailID]" the value "(eMailID)"
And I enter into input field "[PhoneNum]" the value "(PhoneNum)"
And I enter into input field "[MonthlyIncome]" the value "(MonthlyIncome)"
And I enter into input field "[Institution]" the value "(Institution)"
And I enter into input field "[AcctType]" the value "(AcctType)"
And I enter into input field "[CashVal]" the value "(CashVal)"
And I enter into input field "[AcctNum]" the value "(AcctNum)"
And I click on "[Save&Close]" button
Then "[SuccMsg]" should have text as "Thank you! Edit USDN Non-Borrower is complete successfully"

Scenario: 2 This is scenario for testing 
Given I navigate to "[AppURL]"
And I enter into input field "[UID]" the value "(UID)"
And I enter into input field "[PWD]" the value "(PWD)"
When I click on "[Login]" button
And I enter into input field "[LoanNumber]" the value "(LoanNumber)"
When I click on "[GO]" button
And I select from dropdown "[Action]" the value "USDAnonBorrower"
When I click on "[InsertHouseHoldMember]" button
And I enter into input field "[HouseHoldmemberName]" the value "(HouseHoldmemberName)"
And I enter into input field "[SSN]" the value "[SSN]"
And I check the checkbox "[Checkbox]"
And I enter into input field "[eMailID]" the value "(eMailID)"
And I enter into input field "[PhoneNum]" the value "(PhoneNum)"
And I enter into input field "[MonthlyIncome]" the value "(MonthlyIncome)"
And I click on "[Save]" button
Then "[SucessMsg]" should have text as "Your changes are saved sucesfully"

Scenario: 3 This is scenario for testing 
Given I navigate to "[AppURL]"
And I enter into input field "[UID]" the value "(UID)"
And I enter into input field "[PWD]" the value "(PWD)"
When I click on "[Login]" button
And I enter into input field "[LoanNumber]" the value "(LoanNumber)"
When I click on "[GO]" button
And I select from dropdown "[Action]" the value "USDAnonBorrower"
When I click on "[InsertHouseHoldMember]" button
And I enter into input field "[HouseHoldmemberName]" the value "(HouseHoldmemberName)"
And I enter into input field "[SSN]" the value "[SSN]"
And I check the checkbox "[Checkbox]"
And I enter into input field "[eMailID]" the value "(eMailID)"
And I enter into input field "[PhoneNum]" the value "(PhoneNum)"
And I click on "[Save]" button
Then "[InvalidMontlyIncomeMsg]" should have text as "Invalid monthly income entered"

Scenario: 4 This is scenario for testing 
Given I navigate to "[AppURL]"
And I enter into input field "[UID]" the value "(UID)"
And I enter into input field "[PWD]" the value "(PWD)"
When I click on "[Login]" button
And I enter into input field "[LoanNumber]" the value "(LoanNumber)"
When I click on "[GO]" button
And I select from dropdown "[Action]" the value "USDAnonBorrower"
When I click on "[InsertHouseHoldMember]" button
And I enter into input field "[HouseHoldmemberName]" the value "(HouseHoldmemberName)"
And I enter into input field "[SSN]" the value "[SSN]"
And I check the checkbox "[Checkbox]"
And I enter into input field "[eMailID]" the value "(eMailID)"
And I enter into input field "[PhoneNum]" the value "(InvalidPhoneNum)"
And I enter into input field "[MonthlyIncome]" the value "(MonthlyIncome)"
And I click on "[Save]" button
Then "[InvalidPhoneMsg]" should have text as "Invalid phone number entered"

Scenario: 5 This is scenario for testing 
Given I navigate to "[AppURL]"
And I enter into input field "[UID]" the value "(UID)"
And I enter into input field "[PWD]" the value "(PWD)"
When I click on "[Login]" button
And I enter into input field "[LoanNumber]" the value "(LoanNumber)"
When I click on "[GO]" button
And I select from dropdown "[Action]" the value "USDAnonBorrower"
When I click on "[InsertHouseHoldMember]" button
And I enter into input field "[HouseHoldmemberName]" the value "(HouseHoldmemberName)"
And I enter into input field "[SSN]" the value "[SSN]"
And I check the checkbox "[Checkbox]"
And I enter into input field "[eMailID]" the value "(InvalideMailID)"
And I enter into input field "[PhoneNum]" the value "(PhoneNum)"
And I enter into input field "[MonthlyIncome]" the value "(MonthlyIncome)"
And I click on "[Save]" button
Then "[InvalidemailMsg]" should have text as "Invalid email ID entered"

