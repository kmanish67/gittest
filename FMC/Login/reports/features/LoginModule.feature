@FMC @0.1 @FMC_Web @Login
Feature: LoginModule
Given I navigate to "[https://testsales.freedommortgage.com/wps/myportal]"
And I enter into content editable "[UserId]" the value "(ValidUserId)"
And I click on "[LoginButton]" button
Then I should see page title as "[Welcome to the portal , saleslo]"
Scenario: Successful Password Reset using Forget Password Link
Given I navigate to "[https://testsales.freedommortgage.com/wps/myportal]"
And I click on "[forgetPasswordLink]" link
And I enter into content editable "[UserId]" the value "(ValidUserId)"
And I click on "[SubmitButton]" button
And I enter into content editable "[SecurityQuestionField]" the value "(SecurityQuestionValidValue)"
And I enter into content editable "[NewPasswordField]" the value "(NewPasswordValidValue)"
And I enter into content editable "[ConfirmPasswordField]" the value "(ConfirmPasswordValidValue)"
Then I should see page title as "[Password Reset is Successful]"
Scenario: Verify Error on Forget Password Link once New Password Provided having Invalid value
Given I navigate to "[https://testsales.freedommortgage.com/wps/myportal]"
And I click on "[forgetPasswordLink]" link
And I enter into content editable "[UserId]" the value "(ValidUserId)"
And I click on "[SubmitButton]" button
And I enter into content editable "[SecurityQuestionField]" the value "(SecurityQuestionValidValue)"
And I enter into content editable "[NewPasswordField]" the value "(NewPasswordInvalidValue)"
And I click on "[ResetPasswordButton]" button
Then I should see page title as "[Invalid value provided for New Password]"
Scenario: Verify Error on Forget Password Link once Confirm Password Provided having Invalid value
Given I navigate to "[https://testsales.freedommortgage.com/wps/myportal]"
And I click on "[forgetPasswordLink]" link
And I enter into content editable "[UserId]" the value "(ValidUserId)"
And I click on "[SubmitButton]" button
And I enter into content editable "[SecurityQuestionField]" the value "(SecurityQuestionValidValue)"
And I enter into content editable "[NewPasswordField]" the value "(NewPasswordValidValue)"
And I enter into content editable "[ConfirmPasswordField]" the value "(ConfirmPasswordInvalidValue)"
And I click on "[ResetPasswordButton]" button
Then I should see page title as "[Invalid value provided for Confirm Password]"
Scenario: Verify Error on Forget Password Link once Security Question Provided with Invalid value
Given I navigate to "[https://testsales.freedommortgage.com/wps/myportal]"
And I click on "[forgetPasswordLink]" link
And I enter into content editable "[UserId]" the value "(ValidUserId)"
And I click on "[SubmitButton]" button
And I enter into content editable "[SecurityQuestionField]" the value "(InvalidValue)"
Then I should see page title as "[Your response do not match what we have in the file, please try again]"
Scenario: Verify Error on Forget Password Link once Security Question Provided with Blank value
Given I navigate to "[https://testsales.freedommortgage.com/wps/myportal]"
And I click on "[forgetPasswordLink]" link
And I enter into content editable "[UserId]" the value "(ValidUserId)"
And I click on "[SubmitButton]" button
And I enter into content editable "[SecurityQuestionField]" the value "(BlankValue)"
Then I should see page title as "[Please enter the response]"
Scenario: Verify Error on Forget Password Link once Confirm Password Provided with Blank value
Given I navigate to "[https://testsales.freedommortgage.com/wps/myportal]"
And I click on "[forgetPasswordLink]" link
And I enter into content editable "[UserId]" the value "(ValidUserId)"
And I click on "[SubmitButton]" button
And I enter into content editable "[SecurityQuestionField]" the value "(SecurityQuestionValidValue)"
And I enter into content editable "[NewPasswordField]" the value "(NewPasswordValidValue)"
And I click on "[ResetPasswordButton]" button
Then I should see page title as "[Please enter password & confirm password]"
Scenario: Verify Error on Forget Password Link once New Password Provided with Blank value
Given I navigate to "[https://testsales.freedommortgage.com/wps/myportal]"
And I click on "[forgetPasswordLink]" link
And I enter into content editable "[UserId]" the value "(ValidUserId)"
And I click on "[SubmitButton]" button
And I enter into content editable "[SecurityQuestionField]" the value "(SecurityQuestionValidValue)"
And I enter into content editable "[ConfirmPasswordField]" the value "(ConfirmPasswordBlankValue)"
And I click on "[ResetPasswordButton]" button
Then I should see page title as "[Please enter password & confirm password]"
Given I navigate to "[https://testsales.freedommortgage.com/wps/myportal]"
Then I should see page title as "[User authentication failed. Either the user name or password was incorrect.  This User ID does not exist.]"
Given I navigate to "[https://testsales.freedommortgage.com/wps/myportal]"
Then I should see page title as "[User authentication failed. Either the user name or password was incorrect]"
Scenario: Verify Error on Forget Password Link once UserId Provided with Invalid value
Given I navigate to "[https://testsales.freedommortgage.com/wps/myportal]"
And I click on "[forgetPasswordLink]" link
Then I should see page title as "[User data is not currently available: null]"

Scenario: Successful Login using Valid UID and Valid Password
Given I navigate to "[https://testsales.freedommortgage.com/wps/myportal]"
And I enter into content editable "[UserId]" the value "(ValidUserId)"
And I enter into content editable "[PasswordField]" the value "(ValidPasswordValue)"
And I click on "[LoginButton]" button
Then I should see page title as "[Welcome to the portal , saleslo]"
Scenario: Successful Password Reset using Forget Password Link
Given I navigate to "[https://testsales.freedommortgage.com/wps/myportal]"
And I click on "[forgetPasswordLink]" link
And I enter into content editable "[UserId]" the value "(ValidUserId)"
And I click on "[SubmitButton]" button
Then I should see page title as "[Password Reset is Successful]"
Scenario: Verify Error on Forget Password Link once New Password Provided having Invalid value
Given I navigate to "[https://testsales.freedommortgage.com/wps/myportal]"
And I click on "[forgetPasswordLink]" link
And I enter into content editable "[UserId]" the value "(ValidUserId)"
And I click on "[SubmitButton]" button
And I click on "[ResetPasswordButton]" button
Then I should see page title as "[Invalid value provided for New Password]"
Scenario: Verify Error on Forget Password Link once Confirm Password Provided having Invalid value
Given I navigate to "[https://testsales.freedommortgage.com/wps/myportal]"
And I click on "[forgetPasswordLink]" link
And I enter into content editable "[UserId]" the value "(ValidUserId)"
And I click on "[SubmitButton]" button
And I click on "[ResetPasswordButton]" button
Then I should see page title as "[Invalid value provided for Confirm Password]"
Scenario: Verify Error on Forget Password Link once Security Question Provided with Invalid value
Given I navigate to "[https://testsales.freedommortgage.com/wps/myportal]"
And I click on "[forgetPasswordLink]" link
And I enter into content editable "[UserId]" the value "(ValidUserId)"
And I click on "[SubmitButton]" button
Then I should see page title as "[Your response do not match what we have in the file, please try again]"
Scenario: Verify Error on Forget Password Link once Security Question Provided with Blank value
Given I navigate to "[https://testsales.freedommortgage.com/wps/myportal]"
And I click on "[forgetPasswordLink]" link
And I enter into content editable "[UserId]" the value "(ValidUserId)"
And I click on "[SubmitButton]" button
Then I should see page title as "[Please enter the response]"
Scenario: Verify Error on Forget Password Link once Confirm Password Provided with Blank value
Given I navigate to "[https://testsales.freedommortgage.com/wps/myportal]"
And I click on "[forgetPasswordLink]" link
And I enter into content editable "[UserId]" the value "(ValidUserId)"
And I click on "[SubmitButton]" button
And I click on "[ResetPasswordButton]" button
Then I should see page title as "[Please enter password & confirm password]"
Scenario: Verify Error on Forget Password Link once New Password Provided with Blank value
Given I navigate to "[https://testsales.freedommortgage.com/wps/myportal]"
And I click on "[forgetPasswordLink]" link
And I enter into content editable "[UserId]" the value "(ValidUserId)"
And I click on "[SubmitButton]" button
And I click on "[ResetPasswordButton]" button
Then I should see page title as "[Please enter password & confirm password]"
Given I navigate to "[https://testsales.freedommortgage.com/wps/myportal]"
Then I should see page title as "[User authentication failed. Either the user name or password was incorrect.  This User ID does not exist.]"
Given I navigate to "[https://testsales.freedommortgage.com/wps/myportal]"
Then I should see page title as "[User authentication failed. Either the user name or password was incorrect]"
Scenario: Verify Error on Forget Password Link once UserId Provided with Invalid value
Given I navigate to "[https://testsales.freedommortgage.com/wps/myportal]"
And I click on "[forgetPasswordLink]" link
Then I should see page title as "[User data is not currently available: null]"

Given I navigate to "[https://testsales.freedommortgage.com/wps/myportal]"
And I enter into content editable "[UserId]" the value "(ValidUserId)"
And I click on "[LoginButton]" button
Then I should see page title as "[Welcome to the portal , saleslo]"
Scenario: Successful Password Reset using Forget Password Link
Given I navigate to "[https://testsales.freedommortgage.com/wps/myportal]"
And I click on "[forgetPasswordLink]" link
And I enter into content editable "[UserId]" the value "(ValidUserId)"
And I click on "[SubmitButton]" button
And I enter into content editable "[SecurityQuestionField]" the value "(SecurityQuestionValidValue)"
And I enter into content editable "[ConfirmPasswordField]" the value "(ConfirmPasswordValidValue)"
Then I should see page title as "[Password Reset is Successful]"
Scenario: Verify Error on Forget Password Link once New Password Provided having Invalid value
Given I navigate to "[https://testsales.freedommortgage.com/wps/myportal]"
And I click on "[forgetPasswordLink]" link
And I enter into content editable "[UserId]" the value "(ValidUserId)"
And I click on "[SubmitButton]" button
And I enter into content editable "[SecurityQuestionField]" the value "(SecurityQuestionValidValue)"
And I click on "[ResetPasswordButton]" button
Then I should see page title as "[Invalid value provided for New Password]"
Scenario: Verify Error on Forget Password Link once Confirm Password Provided having Invalid value
Given I navigate to "[https://testsales.freedommortgage.com/wps/myportal]"
And I click on "[forgetPasswordLink]" link
And I enter into content editable "[UserId]" the value "(ValidUserId)"
And I click on "[SubmitButton]" button
And I enter into content editable "[SecurityQuestionField]" the value "(SecurityQuestionValidValue)"
And I enter into content editable "[ConfirmPasswordField]" the value "(ConfirmPasswordInvalidValue)"
And I click on "[ResetPasswordButton]" button
Then I should see page title as "[Invalid value provided for Confirm Password]"
Scenario: Verify Error on Forget Password Link once Security Question Provided with Invalid value
Given I navigate to "[https://testsales.freedommortgage.com/wps/myportal]"
And I click on "[forgetPasswordLink]" link
And I enter into content editable "[UserId]" the value "(ValidUserId)"
And I click on "[SubmitButton]" button
And I enter into content editable "[SecurityQuestionField]" the value "(InvalidValue)"
Then I should see page title as "[Your response do not match what we have in the file, please try again]"
Scenario: Verify Error on Forget Password Link once Security Question Provided with Blank value
Given I navigate to "[https://testsales.freedommortgage.com/wps/myportal]"
And I click on "[forgetPasswordLink]" link
And I enter into content editable "[UserId]" the value "(ValidUserId)"
And I click on "[SubmitButton]" button
And I enter into content editable "[SecurityQuestionField]" the value "(BlankValue)"
Then I should see page title as "[Please enter the response]"
Scenario: Verify Error on Forget Password Link once Confirm Password Provided with Blank value
Given I navigate to "[https://testsales.freedommortgage.com/wps/myportal]"
And I click on "[forgetPasswordLink]" link
And I enter into content editable "[UserId]" the value "(ValidUserId)"
And I click on "[SubmitButton]" button
And I enter into content editable "[SecurityQuestionField]" the value "(SecurityQuestionValidValue)"
And I click on "[ResetPasswordButton]" button
Then I should see page title as "[Please enter password & confirm password]"
Scenario: Verify Error on Forget Password Link once New Password Provided with Blank value
Given I navigate to "[https://testsales.freedommortgage.com/wps/myportal]"
And I click on "[forgetPasswordLink]" link
And I enter into content editable "[UserId]" the value "(ValidUserId)"
And I click on "[SubmitButton]" button
And I enter into content editable "[SecurityQuestionField]" the value "(SecurityQuestionValidValue)"
And I enter into content editable "[ConfirmPasswordField]" the value "(ConfirmPasswordBlankValue)"
And I click on "[ResetPasswordButton]" button
Then I should see page title as "[Please enter password & confirm password]"
Given I navigate to "[https://testsales.freedommortgage.com/wps/myportal]"
Then I should see page title as "[User authentication failed. Either the user name or password was incorrect.  This User ID does not exist.]"
Given I navigate to "[https://testsales.freedommortgage.com/wps/myportal]"
Then I should see page title as "[User authentication failed. Either the user name or password was incorrect]"
Scenario: Verify Error on Forget Password Link once UserId Provided with Invalid value
Given I navigate to "[https://testsales.freedommortgage.com/wps/myportal]"
And I click on "[forgetPasswordLink]" link
Then I should see page title as "[User data is not currently available: null]"

Given I navigate to "[https://testsales.freedommortgage.com/wps/myportal]"
And I enter into content editable "[UserId]" the value "(ValidUserId)"
And I click on "[LoginButton]" button
Then I should see page title as "[Welcome to the portal , saleslo]"
Scenario: Successful Password Reset using Forget Password Link
Given I navigate to "[https://testsales.freedommortgage.com/wps/myportal]"
And I click on "[forgetPasswordLink]" link
And I enter into content editable "[UserId]" the value "(ValidUserId)"
And I click on "[SubmitButton]" button
And I enter into content editable "[SecurityQuestionField]" the value "(SecurityQuestionValidValue)"
And I enter into content editable "[NewPasswordField]" the value "(NewPasswordValidValue)"
Then I should see page title as "[Password Reset is Successful]"
Scenario: Verify Error on Forget Password Link once New Password Provided having Invalid value
Given I navigate to "[https://testsales.freedommortgage.com/wps/myportal]"
And I click on "[forgetPasswordLink]" link
And I enter into content editable "[UserId]" the value "(ValidUserId)"
And I click on "[SubmitButton]" button
And I enter into content editable "[SecurityQuestionField]" the value "(SecurityQuestionValidValue)"
And I enter into content editable "[NewPasswordField]" the value "(NewPasswordInvalidValue)"
And I click on "[ResetPasswordButton]" button
Then I should see page title as "[Invalid value provided for New Password]"
Scenario: Verify Error on Forget Password Link once Confirm Password Provided having Invalid value
Given I navigate to "[https://testsales.freedommortgage.com/wps/myportal]"
And I click on "[forgetPasswordLink]" link
And I enter into content editable "[UserId]" the value "(ValidUserId)"
And I click on "[SubmitButton]" button
And I enter into content editable "[SecurityQuestionField]" the value "(SecurityQuestionValidValue)"
And I enter into content editable "[NewPasswordField]" the value "(NewPasswordValidValue)"
And I click on "[ResetPasswordButton]" button
Then I should see page title as "[Invalid value provided for Confirm Password]"
Scenario: Verify Error on Forget Password Link once Security Question Provided with Invalid value
Given I navigate to "[https://testsales.freedommortgage.com/wps/myportal]"
And I click on "[forgetPasswordLink]" link
And I enter into content editable "[UserId]" the value "(ValidUserId)"
And I click on "[SubmitButton]" button
And I enter into content editable "[SecurityQuestionField]" the value "(InvalidValue)"
Then I should see page title as "[Your response do not match what we have in the file, please try again]"
Scenario: Verify Error on Forget Password Link once Security Question Provided with Blank value
Given I navigate to "[https://testsales.freedommortgage.com/wps/myportal]"
And I click on "[forgetPasswordLink]" link
And I enter into content editable "[UserId]" the value "(ValidUserId)"
And I click on "[SubmitButton]" button
And I enter into content editable "[SecurityQuestionField]" the value "(BlankValue)"
Then I should see page title as "[Please enter the response]"
Scenario: Verify Error on Forget Password Link once Confirm Password Provided with Blank value
Given I navigate to "[https://testsales.freedommortgage.com/wps/myportal]"
And I click on "[forgetPasswordLink]" link
And I enter into content editable "[UserId]" the value "(ValidUserId)"
And I click on "[SubmitButton]" button
And I enter into content editable "[SecurityQuestionField]" the value "(SecurityQuestionValidValue)"
And I enter into content editable "[NewPasswordField]" the value "(NewPasswordValidValue)"
And I click on "[ResetPasswordButton]" button
Then I should see page title as "[Please enter password & confirm password]"
Scenario: Verify Error on Forget Password Link once New Password Provided with Blank value
Given I navigate to "[https://testsales.freedommortgage.com/wps/myportal]"
And I click on "[forgetPasswordLink]" link
And I enter into content editable "[UserId]" the value "(ValidUserId)"
And I click on "[SubmitButton]" button
And I enter into content editable "[SecurityQuestionField]" the value "(SecurityQuestionValidValue)"
And I click on "[ResetPasswordButton]" button
Then I should see page title as "[Please enter password & confirm password]"
Given I navigate to "[https://testsales.freedommortgage.com/wps/myportal]"
Then I should see page title as "[User authentication failed. Either the user name or password was incorrect.  This User ID does not exist.]"
Given I navigate to "[https://testsales.freedommortgage.com/wps/myportal]"
Then I should see page title as "[User authentication failed. Either the user name or password was incorrect]"
Scenario: Verify Error on Forget Password Link once UserId Provided with Invalid value
Given I navigate to "[https://testsales.freedommortgage.com/wps/myportal]"
And I click on "[forgetPasswordLink]" link
Then I should see page title as "[User data is not currently available: null]"

Given I navigate to "[https://testsales.freedommortgage.com/wps/myportal]"
And I enter into content editable "[UserId]" the value "(ValidUserId)"
And I click on "[LoginButton]" button
Then I should see page title as "[Welcome to the portal , saleslo]"
Scenario: Successful Password Reset using Forget Password Link
Given I navigate to "[https://testsales.freedommortgage.com/wps/myportal]"
And I click on "[forgetPasswordLink]" link
And I enter into content editable "[UserId]" the value "(ValidUserId)"
And I click on "[SubmitButton]" button
And I enter into content editable "[NewPasswordField]" the value "(NewPasswordValidValue)"
And I enter into content editable "[ConfirmPasswordField]" the value "(ConfirmPasswordValidValue)"
Then I should see page title as "[Password Reset is Successful]"
Scenario: Verify Error on Forget Password Link once New Password Provided having Invalid value
Given I navigate to "[https://testsales.freedommortgage.com/wps/myportal]"
And I click on "[forgetPasswordLink]" link
And I enter into content editable "[UserId]" the value "(ValidUserId)"
And I click on "[SubmitButton]" button
And I enter into content editable "[NewPasswordField]" the value "(NewPasswordInvalidValue)"
And I click on "[ResetPasswordButton]" button
Then I should see page title as "[Invalid value provided for New Password]"
Scenario: Verify Error on Forget Password Link once Confirm Password Provided having Invalid value
Given I navigate to "[https://testsales.freedommortgage.com/wps/myportal]"
And I click on "[forgetPasswordLink]" link
And I enter into content editable "[UserId]" the value "(ValidUserId)"
And I click on "[SubmitButton]" button
And I enter into content editable "[NewPasswordField]" the value "(NewPasswordValidValue)"
And I enter into content editable "[ConfirmPasswordField]" the value "(ConfirmPasswordInvalidValue)"
And I click on "[ResetPasswordButton]" button
Then I should see page title as "[Invalid value provided for Confirm Password]"
Scenario: Verify Error on Forget Password Link once Security Question Provided with Invalid value
Given I navigate to "[https://testsales.freedommortgage.com/wps/myportal]"
And I click on "[forgetPasswordLink]" link
And I enter into content editable "[UserId]" the value "(ValidUserId)"
And I click on "[SubmitButton]" button
Then I should see page title as "[Your response do not match what we have in the file, please try again]"
Scenario: Verify Error on Forget Password Link once Security Question Provided with Blank value
Given I navigate to "[https://testsales.freedommortgage.com/wps/myportal]"
And I click on "[forgetPasswordLink]" link
And I enter into content editable "[UserId]" the value "(ValidUserId)"
And I click on "[SubmitButton]" button
Then I should see page title as "[Please enter the response]"
Scenario: Verify Error on Forget Password Link once Confirm Password Provided with Blank value
Given I navigate to "[https://testsales.freedommortgage.com/wps/myportal]"
And I click on "[forgetPasswordLink]" link
And I enter into content editable "[UserId]" the value "(ValidUserId)"
And I click on "[SubmitButton]" button
And I enter into content editable "[NewPasswordField]" the value "(NewPasswordValidValue)"
And I click on "[ResetPasswordButton]" button
Then I should see page title as "[Please enter password & confirm password]"
Scenario: Verify Error on Forget Password Link once New Password Provided with Blank value
Given I navigate to "[https://testsales.freedommortgage.com/wps/myportal]"
And I click on "[forgetPasswordLink]" link
And I enter into content editable "[UserId]" the value "(ValidUserId)"
And I click on "[SubmitButton]" button
And I enter into content editable "[ConfirmPasswordField]" the value "(ConfirmPasswordBlankValue)"
And I click on "[ResetPasswordButton]" button
Then I should see page title as "[Please enter password & confirm password]"
Given I navigate to "[https://testsales.freedommortgage.com/wps/myportal]"
Then I should see page title as "[User authentication failed. Either the user name or password was incorrect.  This User ID does not exist.]"
Given I navigate to "[https://testsales.freedommortgage.com/wps/myportal]"
Then I should see page title as "[User authentication failed. Either the user name or password was incorrect]"
Scenario: Verify Error on Forget Password Link once UserId Provided with Invalid value
Given I navigate to "[https://testsales.freedommortgage.com/wps/myportal]"
And I click on "[forgetPasswordLink]" link
Then I should see page title as "[User data is not currently available: null]"

Given I navigate to "[https://testsales.freedommortgage.com/wps/myportal]"
And I enter into content editable "[UserId]" the value "(ValidUserId)"
And I click on "[LoginButton]" button
Then I should see page title as "[Welcome to the portal , saleslo]"
Scenario: Successful Password Reset using Forget Password Link
Given I navigate to "[https://testsales.freedommortgage.com/wps/myportal]"
And I click on "[forgetPasswordLink]" link
And I enter into content editable "[UserId]" the value "(ValidUserId)"
And I click on "[SubmitButton]" button
And I enter into content editable "[NewPasswordField]" the value "(NewPasswordValidValue)"
And I enter into content editable "[ConfirmPasswordField]" the value "(ConfirmPasswordValidValue)"
Then I should see page title as "[Password Reset is Successful]"
Scenario: Verify Error on Forget Password Link once New Password Provided having Invalid value
Given I navigate to "[https://testsales.freedommortgage.com/wps/myportal]"
And I click on "[forgetPasswordLink]" link
And I enter into content editable "[UserId]" the value "(ValidUserId)"
And I click on "[SubmitButton]" button
And I enter into content editable "[NewPasswordField]" the value "(NewPasswordInvalidValue)"
And I click on "[ResetPasswordButton]" button
Then I should see page title as "[Invalid value provided for New Password]"
Scenario: Verify Error on Forget Password Link once Confirm Password Provided having Invalid value
Given I navigate to "[https://testsales.freedommortgage.com/wps/myportal]"
And I click on "[forgetPasswordLink]" link
And I enter into content editable "[UserId]" the value "(ValidUserId)"
And I click on "[SubmitButton]" button
And I enter into content editable "[NewPasswordField]" the value "(NewPasswordValidValue)"
And I enter into content editable "[ConfirmPasswordField]" the value "(ConfirmPasswordInvalidValue)"
And I click on "[ResetPasswordButton]" button
Then I should see page title as "[Invalid value provided for Confirm Password]"
Scenario: Verify Error on Forget Password Link once Security Question Provided with Invalid value
Given I navigate to "[https://testsales.freedommortgage.com/wps/myportal]"
And I click on "[forgetPasswordLink]" link
And I enter into content editable "[UserId]" the value "(ValidUserId)"
And I click on "[SubmitButton]" button
Then I should see page title as "[Your response do not match what we have in the file, please try again]"
Scenario: Verify Error on Forget Password Link once Security Question Provided with Blank value
Given I navigate to "[https://testsales.freedommortgage.com/wps/myportal]"
And I click on "[forgetPasswordLink]" link
And I enter into content editable "[UserId]" the value "(ValidUserId)"
And I click on "[SubmitButton]" button
Then I should see page title as "[Please enter the response]"
Scenario: Verify Error on Forget Password Link once Confirm Password Provided with Blank value
Given I navigate to "[https://testsales.freedommortgage.com/wps/myportal]"
And I click on "[forgetPasswordLink]" link
And I enter into content editable "[UserId]" the value "(ValidUserId)"
And I click on "[SubmitButton]" button
And I enter into content editable "[NewPasswordField]" the value "(NewPasswordValidValue)"
And I click on "[ResetPasswordButton]" button
Then I should see page title as "[Please enter password & confirm password]"
Scenario: Verify Error on Forget Password Link once New Password Provided with Blank value
Given I navigate to "[https://testsales.freedommortgage.com/wps/myportal]"
And I click on "[forgetPasswordLink]" link
And I enter into content editable "[UserId]" the value "(ValidUserId)"
And I click on "[SubmitButton]" button
And I enter into content editable "[ConfirmPasswordField]" the value "(ConfirmPasswordBlankValue)"
And I click on "[ResetPasswordButton]" button
Then I should see page title as "[Please enter password & confirm password]"
Given I navigate to "[https://testsales.freedommortgage.com/wps/myportal]"
Then I should see page title as "[User authentication failed. Either the user name or password was incorrect.  This User ID does not exist.]"
Given I navigate to "[https://testsales.freedommortgage.com/wps/myportal]"
Then I should see page title as "[User authentication failed. Either the user name or password was incorrect]"
Scenario: Verify Error on Forget Password Link once UserId Provided with Invalid value
Given I navigate to "[https://testsales.freedommortgage.com/wps/myportal]"
And I click on "[forgetPasswordLink]" link
Then I should see page title as "[User data is not currently available: null]"

Given I navigate to "[https://testsales.freedommortgage.com/wps/myportal]"
And I enter into content editable "[UserId]" the value "(ValidUserId)"
And I click on "[LoginButton]" button
Then I should see page title as "[Welcome to the portal , saleslo]"
Scenario: Successful Password Reset using Forget Password Link
Given I navigate to "[https://testsales.freedommortgage.com/wps/myportal]"
And I click on "[forgetPasswordLink]" link
And I enter into content editable "[UserId]" the value "(ValidUserId)"
And I click on "[SubmitButton]" button
And I enter into content editable "[SecurityQuestionField]" the value "(SecurityQuestionValidValue)"
And I enter into content editable "[NewPasswordField]" the value "(NewPasswordValidValue)"
Then I should see page title as "[Password Reset is Successful]"
Scenario: Verify Error on Forget Password Link once New Password Provided having Invalid value
Given I navigate to "[https://testsales.freedommortgage.com/wps/myportal]"
And I click on "[forgetPasswordLink]" link
And I enter into content editable "[UserId]" the value "(ValidUserId)"
And I click on "[SubmitButton]" button
And I enter into content editable "[SecurityQuestionField]" the value "(SecurityQuestionValidValue)"
And I enter into content editable "[NewPasswordField]" the value "(NewPasswordInvalidValue)"
And I click on "[ResetPasswordButton]" button
Then I should see page title as "[Invalid value provided for New Password]"
Scenario: Verify Error on Forget Password Link once Confirm Password Provided having Invalid value
Given I navigate to "[https://testsales.freedommortgage.com/wps/myportal]"
And I click on "[forgetPasswordLink]" link
And I enter into content editable "[UserId]" the value "(ValidUserId)"
And I click on "[SubmitButton]" button
And I enter into content editable "[SecurityQuestionField]" the value "(SecurityQuestionValidValue)"
And I enter into content editable "[NewPasswordField]" the value "(NewPasswordValidValue)"
And I click on "[ResetPasswordButton]" button
Then I should see page title as "[Invalid value provided for Confirm Password]"
Scenario: Verify Error on Forget Password Link once Security Question Provided with Invalid value
Given I navigate to "[https://testsales.freedommortgage.com/wps/myportal]"
And I click on "[forgetPasswordLink]" link
And I enter into content editable "[UserId]" the value "(ValidUserId)"
And I click on "[SubmitButton]" button
And I enter into content editable "[SecurityQuestionField]" the value "(InvalidValue)"
Then I should see page title as "[Your response do not match what we have in the file, please try again]"
Scenario: Verify Error on Forget Password Link once Security Question Provided with Blank value
Given I navigate to "[https://testsales.freedommortgage.com/wps/myportal]"
And I click on "[forgetPasswordLink]" link
And I enter into content editable "[UserId]" the value "(ValidUserId)"
And I click on "[SubmitButton]" button
And I enter into content editable "[SecurityQuestionField]" the value "(BlankValue)"
Then I should see page title as "[Please enter the response]"
Scenario: Verify Error on Forget Password Link once Confirm Password Provided with Blank value
Given I navigate to "[https://testsales.freedommortgage.com/wps/myportal]"
And I click on "[forgetPasswordLink]" link
And I enter into content editable "[UserId]" the value "(ValidUserId)"
And I click on "[SubmitButton]" button
And I enter into content editable "[SecurityQuestionField]" the value "(SecurityQuestionValidValue)"
And I enter into content editable "[NewPasswordField]" the value "(NewPasswordValidValue)"
And I enter into content editable "[ConfirmPasswordField]" the value "(ConfirmPasswordBlankValue)"
And I click on "[ResetPasswordButton]" button
Then I should see page title as "[Please enter password & confirm password]"
Scenario: Verify Error on Forget Password Link once New Password Provided with Blank value
Given I navigate to "[https://testsales.freedommortgage.com/wps/myportal]"
And I click on "[forgetPasswordLink]" link
And I enter into content editable "[UserId]" the value "(ValidUserId)"
And I click on "[SubmitButton]" button
And I enter into content editable "[SecurityQuestionField]" the value "(SecurityQuestionValidValue)"
And I click on "[ResetPasswordButton]" button
Then I should see page title as "[Please enter password & confirm password]"
Given I navigate to "[https://testsales.freedommortgage.com/wps/myportal]"
Then I should see page title as "[User authentication failed. Either the user name or password was incorrect.  This User ID does not exist.]"
Given I navigate to "[https://testsales.freedommortgage.com/wps/myportal]"
Then I should see page title as "[User authentication failed. Either the user name or password was incorrect]"
Scenario: Verify Error on Forget Password Link once UserId Provided with Invalid value
Given I navigate to "[https://testsales.freedommortgage.com/wps/myportal]"
And I click on "[forgetPasswordLink]" link
Then I should see page title as "[User data is not currently available: null]"

Given I navigate to "[https://testsales.freedommortgage.com/wps/myportal]"
And I enter into content editable "[UserId]" the value "(ValidUserId)"
And I click on "[LoginButton]" button
Then I should see page title as "[Welcome to the portal , saleslo]"
Scenario: Successful Password Reset using Forget Password Link
Given I navigate to "[https://testsales.freedommortgage.com/wps/myportal]"
And I click on "[forgetPasswordLink]" link
And I enter into content editable "[UserId]" the value "(ValidUserId)"
And I click on "[SubmitButton]" button
And I enter into content editable "[SecurityQuestionField]" the value "(SecurityQuestionValidValue)"
And I enter into content editable "[ConfirmPasswordField]" the value "(ConfirmPasswordValidValue)"
Then I should see page title as "[Password Reset is Successful]"
Scenario: Verify Error on Forget Password Link once New Password Provided having Invalid value
Given I navigate to "[https://testsales.freedommortgage.com/wps/myportal]"
And I click on "[forgetPasswordLink]" link
And I enter into content editable "[UserId]" the value "(ValidUserId)"
And I click on "[SubmitButton]" button
And I enter into content editable "[SecurityQuestionField]" the value "(SecurityQuestionValidValue)"
And I click on "[ResetPasswordButton]" button
Then I should see page title as "[Invalid value provided for New Password]"
Scenario: Verify Error on Forget Password Link once Confirm Password Provided having Invalid value
Given I navigate to "[https://testsales.freedommortgage.com/wps/myportal]"
And I click on "[forgetPasswordLink]" link
And I enter into content editable "[UserId]" the value "(ValidUserId)"
And I click on "[SubmitButton]" button
And I enter into content editable "[SecurityQuestionField]" the value "(SecurityQuestionValidValue)"
And I enter into content editable "[ConfirmPasswordField]" the value "(ConfirmPasswordInvalidValue)"
And I click on "[ResetPasswordButton]" button
Then I should see page title as "[Invalid value provided for Confirm Password]"
Scenario: Verify Error on Forget Password Link once Security Question Provided with Invalid value
Given I navigate to "[https://testsales.freedommortgage.com/wps/myportal]"
And I click on "[forgetPasswordLink]" link
And I enter into content editable "[UserId]" the value "(ValidUserId)"
And I click on "[SubmitButton]" button
And I enter into content editable "[SecurityQuestionField]" the value "(InvalidValue)"
Then I should see page title as "[Your response do not match what we have in the file, please try again]"
Scenario: Verify Error on Forget Password Link once Security Question Provided with Blank value
Given I navigate to "[https://testsales.freedommortgage.com/wps/myportal]"
And I click on "[forgetPasswordLink]" link
And I enter into content editable "[UserId]" the value "(ValidUserId)"
And I click on "[SubmitButton]" button
And I enter into content editable "[SecurityQuestionField]" the value "(BlankValue)"
Then I should see page title as "[Please enter the response]"
Scenario: Verify Error on Forget Password Link once Confirm Password Provided with Blank value
Given I navigate to "[https://testsales.freedommortgage.com/wps/myportal]"
And I click on "[forgetPasswordLink]" link
And I enter into content editable "[UserId]" the value "(ValidUserId)"
And I click on "[SubmitButton]" button
And I enter into content editable "[SecurityQuestionField]" the value "(SecurityQuestionValidValue)"
And I click on "[ResetPasswordButton]" button
Then I should see page title as "[Please enter password & confirm password]"
Scenario: Verify Error on Forget Password Link once New Password Provided with Blank value
Given I navigate to "[https://testsales.freedommortgage.com/wps/myportal]"
And I click on "[forgetPasswordLink]" link
And I enter into content editable "[UserId]" the value "(ValidUserId)"
And I click on "[SubmitButton]" button
And I enter into content editable "[SecurityQuestionField]" the value "(SecurityQuestionValidValue)"
And I enter into content editable "[NewPasswordField]" the value "(NewPasswordBlankValue)"
And I enter into content editable "[ConfirmPasswordField]" the value "(ConfirmPasswordBlankValue)"
And I click on "[ResetPasswordButton]" button
Then I should see page title as "[Please enter password & confirm password]"
Given I navigate to "[https://testsales.freedommortgage.com/wps/myportal]"
Then I should see page title as "[User authentication failed. Either the user name or password was incorrect.  This User ID does not exist.]"
Given I navigate to "[https://testsales.freedommortgage.com/wps/myportal]"
Then I should see page title as "[User authentication failed. Either the user name or password was incorrect]"
Scenario: Verify Error on Forget Password Link once UserId Provided with Invalid value
Given I navigate to "[https://testsales.freedommortgage.com/wps/myportal]"
And I click on "[forgetPasswordLink]" link
Then I should see page title as "[User data is not currently available: null]"

Given I navigate to "[https://testsales.freedommortgage.com/wps/myportal]"
And I enter into content editable "[UserId]" the value "(ValidUserId)"
And I click on "[LoginButton]" button
Then I should see page title as "[Welcome to the portal , saleslo]"
Scenario: Successful Password Reset using Forget Password Link
Given I navigate to "[https://testsales.freedommortgage.com/wps/myportal]"
And I click on "[forgetPasswordLink]" link
And I enter into content editable "[UserId]" the value "(ValidUserId)"
And I click on "[SubmitButton]" button
Then I should see page title as "[Password Reset is Successful]"
Scenario: Verify Error on Forget Password Link once New Password Provided having Invalid value
Given I navigate to "[https://testsales.freedommortgage.com/wps/myportal]"
And I click on "[forgetPasswordLink]" link
And I enter into content editable "[UserId]" the value "(ValidUserId)"
And I click on "[SubmitButton]" button
And I click on "[ResetPasswordButton]" button
Then I should see page title as "[Invalid value provided for New Password]"
Scenario: Verify Error on Forget Password Link once Confirm Password Provided having Invalid value
Given I navigate to "[https://testsales.freedommortgage.com/wps/myportal]"
And I click on "[forgetPasswordLink]" link
And I enter into content editable "[UserId]" the value "(ValidUserId)"
And I click on "[SubmitButton]" button
And I click on "[ResetPasswordButton]" button
Then I should see page title as "[Invalid value provided for Confirm Password]"
Scenario: Verify Error on Forget Password Link once Security Question Provided with Invalid value
Given I navigate to "[https://testsales.freedommortgage.com/wps/myportal]"
And I click on "[forgetPasswordLink]" link
And I enter into content editable "[UserId]" the value "(ValidUserId)"
And I click on "[SubmitButton]" button
Then I should see page title as "[Your response do not match what we have in the file, please try again]"
Scenario: Verify Error on Forget Password Link once Security Question Provided with Blank value
Given I navigate to "[https://testsales.freedommortgage.com/wps/myportal]"
And I click on "[forgetPasswordLink]" link
And I enter into content editable "[UserId]" the value "(ValidUserId)"
And I click on "[SubmitButton]" button
Then I should see page title as "[Please enter the response]"
Scenario: Verify Error on Forget Password Link once Confirm Password Provided with Blank value
Given I navigate to "[https://testsales.freedommortgage.com/wps/myportal]"
And I click on "[forgetPasswordLink]" link
And I enter into content editable "[UserId]" the value "(ValidUserId)"
And I click on "[SubmitButton]" button
And I click on "[ResetPasswordButton]" button
Then I should see page title as "[Please enter password & confirm password]"
Scenario: Verify Error on Forget Password Link once New Password Provided with Blank value
Given I navigate to "[https://testsales.freedommortgage.com/wps/myportal]"
And I click on "[forgetPasswordLink]" link
And I enter into content editable "[UserId]" the value "(ValidUserId)"
And I click on "[SubmitButton]" button
And I click on "[ResetPasswordButton]" button
Then I should see page title as "[Please enter password & confirm password]"
Given I navigate to "[https://testsales.freedommortgage.com/wps/myportal]"
Then I should see page title as "[User authentication failed. Either the user name or password was incorrect.  This User ID does not exist.]"
Scenario: Verify Error on Login Page when invalid value provided for Password
Given I navigate to "[https://testsales.freedommortgage.com/wps/myportal]"
And I enter into content editable "[UserId]" the value "(InvalidUserId)"
Then I should see page title as "[User authentication failed. Either the user name or password was incorrect]"
Scenario: Verify Error on Forget Password Link once UserId Provided with Invalid value
Given I navigate to "[https://testsales.freedommortgage.com/wps/myportal]"
And I click on "[forgetPasswordLink]" link
Then I should see page title as "[User data is not currently available: null]"

Given I navigate to "[https://testsales.freedommortgage.com/wps/myportal]"
And I enter into content editable "[PasswordField]" the value "(ValidPasswordValue)"
And I click on "[LoginButton]" button
Then I should see page title as "[Welcome to the portal , saleslo]"
Scenario: Successful Password Reset using Forget Password Link
Given I navigate to "[https://testsales.freedommortgage.com/wps/myportal]"
And I click on "[forgetPasswordLink]" link
And I click on "[SubmitButton]" button
Then I should see page title as "[Password Reset is Successful]"
Scenario: Verify Error on Forget Password Link once New Password Provided having Invalid value
Given I navigate to "[https://testsales.freedommortgage.com/wps/myportal]"
And I click on "[forgetPasswordLink]" link
And I click on "[SubmitButton]" button
And I click on "[ResetPasswordButton]" button
Then I should see page title as "[Invalid value provided for New Password]"
Scenario: Verify Error on Forget Password Link once Confirm Password Provided having Invalid value
Given I navigate to "[https://testsales.freedommortgage.com/wps/myportal]"
And I click on "[forgetPasswordLink]" link
And I click on "[SubmitButton]" button
And I click on "[ResetPasswordButton]" button
Then I should see page title as "[Invalid value provided for Confirm Password]"
Scenario: Verify Error on Forget Password Link once Security Question Provided with Invalid value
Given I navigate to "[https://testsales.freedommortgage.com/wps/myportal]"
And I click on "[forgetPasswordLink]" link
And I click on "[SubmitButton]" button
Then I should see page title as "[Your response do not match what we have in the file, please try again]"
Scenario: Verify Error on Forget Password Link once Security Question Provided with Blank value
Given I navigate to "[https://testsales.freedommortgage.com/wps/myportal]"
And I click on "[forgetPasswordLink]" link
And I click on "[SubmitButton]" button
Then I should see page title as "[Please enter the response]"
Scenario: Verify Error on Forget Password Link once Confirm Password Provided with Blank value
Given I navigate to "[https://testsales.freedommortgage.com/wps/myportal]"
And I click on "[forgetPasswordLink]" link
And I click on "[SubmitButton]" button
And I click on "[ResetPasswordButton]" button
Then I should see page title as "[Please enter password & confirm password]"
Scenario: Verify Error on Forget Password Link once New Password Provided with Blank value
Given I navigate to "[https://testsales.freedommortgage.com/wps/myportal]"
And I click on "[forgetPasswordLink]" link
And I click on "[SubmitButton]" button
And I click on "[ResetPasswordButton]" button
Then I should see page title as "[Please enter password & confirm password]"
Scenario: Verify Error on Login Page when invalid value provided for UserId
Given I navigate to "[https://testsales.freedommortgage.com/wps/myportal]"
And I enter into content editable "[UserId]" the value "(InvalidUserId)"
Then I should see page title as "[User authentication failed. Either the user name or password was incorrect.  This User ID does not exist.]"
Given I navigate to "[https://testsales.freedommortgage.com/wps/myportal]"
Then I should see page title as "[User authentication failed. Either the user name or password was incorrect]"
Scenario: Verify Error on Forget Password Link once UserId Provided with Invalid value
Given I navigate to "[https://testsales.freedommortgage.com/wps/myportal]"
And I click on "[forgetPasswordLink]" link
And I enter into content editable "[UserId]" the value "(ValidUserId)"
Then I should see page title as "[User data is not currently available: null]"

