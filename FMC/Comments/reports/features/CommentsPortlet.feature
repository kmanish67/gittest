@FMC @0.1 @FMC_Web @CommentsPortlet
Feature: Comments
Scenario: 1   This is a scenario to test add comments from LKW  for comments portlet. Adding Comments from web portlet is through NA with Valid comments value
Given I navigate to "[LKWURL]"
And I click on "[TestLKW]" button
And I enter into input field "[LKWUID]" the value "(LKWUID)"
And I enter into input field "[LKWPWD]" the value "(LKWPWD)"
And I click on "[LKWSubmit]" button
And I enter into input field "[SelectEnvtocheck]" the value "(SelectEnvtocheck)"
And I click on "[LKWEnter]" button
And I click on "[RetailMortgageProcessing]" button
And I click on "[RetailProcessingMenu]" button
And I enter into input field "[Search]" the value "(LoanNum)"
And I click on "[2CommentsLog]" link
And I click on "[1BrokerComments]" link
And I click on "[1ExternalBrokerComments]" link
And I capture screenshot
Then "[Comments]" should have text as "(CommentsenteredinWeb)"
And I click on "[Logoff]" link
And I close browser

Scenario: 2   This is a scenario to test add comments from Web  for comments portlet. Adding Comments from web portlet is through EditLoan with Valid comments value
Given I navigate to "[AppURL]"
And I enter into input field "[UserName]" the value "(UserName)"
And I enter into input field "[Password]" the value "(Password)"
When I click on "[Login]" button
And I enter into input field "[LoanNumber]" the value "(LoanNumber)"
When I click on "[GO]" button
When I click on "[EditLoan]" button
And I scroll to end of page
And I click on "[AddComment]" button
And I enter into input field "[NewComments]" the value "(NewComments)"
And I check the checkbox "[eMailNotificaiton]"
And I click on "[Submit]" button
Then "[SuccComments]" should have text as "Comment added Successfully"
When I click on "[Logoff]" link
And I close browser
Given I navigate to "[LKWURL]"
And I click on "[TestLKW]" button
And I enter into input field "[LKWUID]" the value "(LKWUID)"
And I enter into input field "[LKWPWD]" the value "(LKWPWD)"
And I click on "[LKWSubmit]" button
And I enter into input field "[SelectEnvtocheck]" the value "(SelectEnvtocheck)"
And I click on "[LKWEnter]" button
And I click on "[RetailMortgageProcessing]" button
And I click on "[RetailProcessingMenu]" button
And I enter into input field "[Search]" the value "(LoanNum)"
And I click on "[2CommentsLog]" link
And I click on "[1BrokerComments]" link
And I click on "[1ExternalBrokerComments]" link
And I capture screenshot
Then "[Comments]" should have text as "(CommentsenteredinWeb)"
And I click on "[Logoff]" link
And I close browser

Scenario: 3   This is a scenario to test add comments from Web  for comments portlet. Adding Comments from web portlet is through ActionLink with Valid comments value
Given I navigate to "[AppURL]"
And I enter into input field "[UserName]" the value "(UserName)"
And I enter into input field "[Password]" the value "(Password)"
When I click on "[Login]" button
And I enter into input field "[LoanNumber]" the value "(LoanNumber)"
When I click on "[GO]" button
When I click on "[Comments]" button
And I click on "[AddComment]" button
And I enter into input field "[NewComments]" the value "(NewComments)"
And I check the checkbox "[eMailNotificaiton]"
And I click on "[Submit]" button
Then "[SuccComments]" should have text as "Comment added Successfully"
When I click on "[Logoff]" link
And I close browser
Given I navigate to "[LKWURL]"
And I click on "[TestLKW]" button
And I enter into input field "[LKWUID]" the value "(LKWUID)"
And I enter into input field "[LKWPWD]" the value "(LKWPWD)"
And I click on "[LKWSubmit]" button
And I enter into input field "[SelectEnvtocheck]" the value "(SelectEnvtocheck)"
And I click on "[LKWEnter]" button
And I click on "[RetailMortgageProcessing]" button
And I click on "[RetailProcessingMenu]" button
And I enter into input field "[Search]" the value "(LoanNum)"
And I click on "[2CommentsLog]" link
And I click on "[1BrokerComments]" link
And I click on "[1ExternalBrokerComments]" link
And I capture screenshot
Then "[Comments]" should have text as "(CommentsenteredinWeb)"
And I click on "[Logoff]" link
And I close browser

Scenario: 4   This is a scenario to test add comments from Web  for comments portlet. Adding Comments from web portlet is through ActionLink with InValid comments value
Given I navigate to "[AppURL]"
And I enter into input field "[UserName]" the value "(UserName)"
And I enter into input field "[Password]" the value "(Password)"
When I click on "[Login]" button
And I enter into input field "[LoanNumber]" the value "(LoanNumber)"
When I click on "[GO]" button
When I click on "[Comments]" button
And I click on "[AddComment]" button
And I click on "[Submit]" button
Then "[InvalidComments]" should have text as "Comments should be entered to submit to lakewood"
When I click on "[Logoff]" link
And I close browser

