@FMC @0.2 @FMC_API @FMCPOCGetUser
Feature: GetFMCAPI
Scenario: SCN 1 goodpath
Given I declare rest service with end point "(Endpointurl)" using method "Get"
And I invoke rest service
And I download the response
And I validate response present
Then I validate response status code with value "200_OK"
And I validate response time less than "(ResponseTime)" in milliseconds
And I validate response contains the value "(lastName)"
And I validate response contains the value "(firstName)"
And I validate response contains the value "(email)"
And I validate response contains the value "(title)"
Then I validate response contains the value "(phone)"
And I validate response contains the value "(fax)"
And I validate response contains the value "(cell)"

Scenario: SCN 2 badpath
Given I declare rest service with end point "(Endpointurl)" using method "Get"
And I invoke rest service
And I download the response
And I validate response present
Then I validate response status code with value "500_InternalServerError"

Scenario: SCN 3 badpath
Given I declare rest service with end point "(Endpointurl)" using method "Get"
And I invoke rest service
And I download the response
And I validate response present
Then I validate response status code with value "404_NotFound"

Scenario: SCN 4 badpath
Given I declare rest service with end point "(Endpointurl)" using method "Get"
And I invoke rest service
And I download the response
And I validate response present
Then I validate response status code with value "401_Unauthorized"

Scenario: SCN 5 badpath
Given I declare rest service with end point "(Endpointurl)" using method "Get"
And I invoke rest service
And I download the response
And I validate response present
Then I validate response status code with value "403_Forbidden"

