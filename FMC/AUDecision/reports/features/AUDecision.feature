@FMC @0.1 @FMC_Web @AUDecisionWeb
Feature: AUDecisions
Scenario: 1  This tese case is for checking 
Given I navigate to "[LKWURL]"
And I click on "[TestLKW]" button
And I enter into input field "[LKWUID]" the value "(LKWUID)"
And I enter into input field "[LKWPWD]" the value "(LKWPWD)"
And I click on "[LKWSubmit]" button
And I enter into input field "[SelectEnvtocheck]" the value "(SelectEnvtocheck)"
And I click on "[LKWEnter]" button
And I click on "[LKWEnter]" button
And I click on "[RetailMortgageProcessing]" button
And I click on "[RetailProcessingMenu]" button
And I click on "[Search]" button
And I click on "[10Underwriting]" button
And I click on "[10AutomatedUnderwriting]" button
And I click on "[2RequestDUunderwriting]" button
And I click on "[UserDesktopUnderwitter]" button
And I enter into input field "[Credit Agency]" the value "Use Exiting"
And I click on "Create and send request" link
And I click on "[1AutomatedUnderwritingInfo]" button
Then "[DU Recommendation]" should contain one value of "(Approve/Eligible)"
And "[AUDAllowed]" should have text as "DU"
And "[UpdateDate]" should have text as "CurrentDate"
And "[updateBy]" should have text as "UserLoggedin"
And I click on "[Logoff]" link
And I close browser
Given I navigate to "[EDMSurl]"
And I enter into input field "[EDMSUID]" the value "(EDMSUID)"
And I enter into input field "[EDMSPWD]" the value "(EDMSPWD)"
And I click on "[EDMSLogin]" button
And I click on "[DocumentManagement]" link
And I enter into input field "[EDMSLoanNum]" the value "(LoanNum)"
And I click on "[FindLoan]" button
And I click on "[Default]" button
And I click on "[Submission]" button
And I click on "[AUFinding]" button
And I click on "[OpenPDF]" button
Then "[CaseID]" should have text as "CaseID"
And I click on "[Logoff]" link
And I close browser

Scenario: 2  This tese case is for checking 
Given I navigate to "(AppURL)"
And I enter into input field "[UserName]" the value "(UserName)"
And I enter into input field "[Password]" the value "(Password)"
When I click on "[Login]" button
And I enter into input field "[LoanNumber]" the value "(LoanNumber)"
And I click on "[GO]" button
And I click on "[CreditDirect]" link
And I select from dropdown "[Borrower]" the value "(BorrowerValue)"
And I click on "[OrderCredit]" link
And I enter into input field "[LoanNumber]" the value "(LoanNumber)"
And I click on "[GO]" button
And I click on "[OrderLPA]" link
And I select radio "Reissue" with the option "(CreditReportType)"
And I click on "[Continue]" link
And I select from dropdown "[CreditAgency]" the value "(CreditAgency)"
And I click on "[RequestUnderWritter]" button
And I click on "[ViewAUFindingCredit]" button
When I click on "[Finding]" link
And I click on "[Credit]" button
And I click on "[DocChecklist]" button
And I click on "[Logout]" link
And I close browser
Given I navigate to "[EDMSurl]"
And I enter into input field "[EDMSUID]" the value "(EDMSUID)"
And I enter into input field "[EDMSPWD]" the value "(EDMSPWD)"
And I click on "[EDMSLogin]" button
And I click on "[DocumentManagement]" link
And I enter into input field "[EDMSLoanNum]" the value "(LoanNum)"
And I click on "[FindLoan]" button
And I click on "[Default]" button
And I click on "[Submission]" button
And I click on "[AUFinding]" button
And I click on "[OpenPDF]" button
Then "[CaseID]" should have text as "CaseID"
And I click on "[Logoff]" link
And I close browser

Scenario: 3  This tese case is for checking 
Given I navigate to "(AppURL)"
And I enter into input field "[UserName]" the value "(UserName)"
And I enter into input field "[Password]" the value "(Password)"
When I click on "[Login]" button
And I enter into input field "[LoanNumber]" the value "(LoanNumber)"
And I click on "[GO]" button
And I click on "[CreditDirect]" link
And I select from dropdown "[Borrower]" the value "(BorrowerValue)"
And I click on "[OrderCredit]" link
And I click on "[ReturnPipeline]" button
And I enter into input field "[LoanNumber]" the value "(LoanNumber)"
And I click on "[GO]" button
And I click on "[OrderDU]" link
And I select radio "Existing" with the option "(CreditReportType)"
And I click on "[Continue]" link
And I select from dropdown "[CreditAgency]" the value "(CreditAgency)"
And I click on "[RequestUnderWritter]" button
And I click on "[ViewAUFindingCredit]" button
When I click on "[Finding]" link
And I click on "[Credit]" button
And I click on "[DocChecklist]" button
And I click on "[Logout]" link
And I close browser
Given I navigate to "[EDMSurl]"
And I enter into input field "[EDMSUID]" the value "(EDMSUID)"
And I enter into input field "[EDMSPWD]" the value "(EDMSPWD)"
And I click on "[EDMSLogin]" button
And I click on "[DocumentManagement]" link
And I enter into input field "[EDMSLoanNum]" the value "(LoanNum)"
And I click on "[FindLoan]" button
And I click on "[Default]" button
And I click on "[Submission]" button
And I click on "[AUFinding]" button
And I click on "[OpenPDF]" button
Then "[CaseID]" should have text as "CaseID"
And I click on "[Logoff]" link
And I close browser

Scenario: 4  This tese case is for checking 
Given I navigate to "(AppURL)"
And I enter into input field "[UserName]" the value "(UserName)"
And I enter into input field "[Password]" the value "(Password)"
When I click on "[Login]" button
And I enter into input field "[LoanNumber]" the value "(LoanNumber)"
And I click on "[GO]" button
And I click on "[OrderDU]" link
And I select radio "Reissue" with the option "(CreditReportType)"
And I click on "[Continue]" link
And I select from dropdown "[ErrorMsg]" the value "No Credit Direct. Loan Reqd to have credit reports"
And I click on "[Logout]" link
And I close browser

