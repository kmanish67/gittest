@WebApp @1 @WebApp_NewFun @EquitableSearch
Feature: EquitableSearch
Scenario: 1  goodpath
Given I navigate to "https://equitable.com/find-financial-professional"
Then "Equitable" should be present
And "Find a financial professional" should be present
And I take screenshot
And I select from dropdown "[SearchBy]" the element having value "Zipcode"
Then "Within" should be present
And I enter into input field "[Zipcode]" the value "08054"
And I click on "[EuqutableGO]" button
And I take screenshot

Scenario: 2  goodpath
Given I navigate to "https://equitable.com/find-financial-professional"
Then "Equitable" should be present
And "Find a financial professional" should be present
And I take screenshot
And I select from dropdown "[SearchBy]" the element having value "Name"
And I select from dropdown "[SearchBy]" the value "Name"
And I enter into input field "[FirstName]" the value "(FirstName)"
And I enter into input field "[LastName]" the value "(LastName)"
And I enter into input field "[City]" the value "Juneau"
And I select from dropdown "[State]" the value "Alaska"
And I click on "[EuqutableGO]" button
And I take screenshot
Then "[LastName]" should contain one value of "(LastName)"

Scenario: 3  goodpath
Given I navigate to "https://equitable.com/find-financial-professional"
Then "Equitable" should be present
And "Find a financial professional" should be present
And I take screenshot
And I select from dropdown "[SearchBy]" the element having value "CityState"
And I select from dropdown "[SearchBy]" the value "CityState"
And I select from dropdown "[WithIn]" the value "50 miles"
And I enter into input field "[City]" the value "Juneau"
And I select from dropdown "[State]" the value "Alaska"
And I take screenshot
Then "CityState" should contain one value of "time"

Scenario: 4  goodpath
Given I navigate to "https://equitable.com/find-financial-professional"
Then "Equitable" should be present
And "Find a financial professional" should be present
And I take screenshot
And I select from dropdown "[SearchBy]" the element having value "Branch"
And I select from dropdown "[SearchBy]" the value "Branch"
And I select from dropdown "[State]" the value "Alaska"
And I click on "[EuqutableGO]" button
And I take screenshot
Then "[State]" should contain one value of "(State)"

Scenario: 5  badpath
Given I navigate to "https://equitable.com/find-financial-professional"
Then "Equitable" should be present
And "Find a financial professional" should be present
And I take screenshot
And I select from dropdown "[SearchBy]" the element having value "Name"
And I select from dropdown "[SearchBy]" the value "Name"
And I enter into input field "[FirstName]" the value "(FirstName)"
And I enter into input field "[LastName]" the value "(LastName)"
And I enter into input field "[City]" the value "Juneau"
And I select from dropdown "[State]" the value "Alaska"
And I click on "[EuqutableGO]" button
And I take screenshot
Then "[LastName]" should contain one value of "(LastName)"

Scenario: 6  badpath
Given I navigate to "https://equitable.com/find-financial-professional"
Then "Equitable" should be present
And "Find a financial professional" should be present
And I take screenshot
And I select from dropdown "[SearchBy]" the element having value "Name"
And I select from dropdown "[SearchBy]" the value "Name"
And I enter into input field "[FirstName]" the value "(FirstName)"
And I enter into input field "[LastName]" the value "(LastName)"
And I enter into input field "[City]" the value "Juneau"
And I select from dropdown "[State]" the value "Alaska"
And I click on "[EuqutableGO]" button
And I take screenshot
Then "[LastName]" should contain one value of "(LastName)"
And I select from dropdown "[SearchBy]" the value "Name"
And I enter into input field "[FirstName]" the value "(FirstName)"
And I enter into input field "[City]" the value "(City)"
And I select from dropdown "[State]" the value "Alaska"
And I click on "[EuqutableGO]" button
Then "Please fill out this field" should be present
And I take screenshot

Scenario: 7  badpath
Given I navigate to "https://equitable.com/find-financial-professional"
Then "Equitable" should be present
And "Find a financial professional" should be present
And I take screenshot
And I select from dropdown "[SearchBy]" the element having value "Zipcode"
Then "Within" should be present
And I enter into input field "[Zipcode]" the value "08054"
And I click on "[EuqutableGO]" button
And I take screenshot
And I select from dropdown "[SearchBy]" the value "Zipcode"
And I click on "[EuqutableGO]" button
Then "Please fill out this field" should be present
And I take screenshot

