@WebApp @0.1 @WebApp_NewFun @WestField
Feature: WestField
Scenario: 1 goodpath
Given I navigate to "https://www.westfield.com/"
And I click on "[Location]" link
And I click on "[Stores]" link
And I click on "[AllStores]" link
And I click on "AllStores" link
And I capture screenshot

Scenario: 2 goodpath
Given I navigate to "https://www.westfield.com/"
And I click on "[Location]" link
And I click on "[Dinning]" link
And I click on "[AllDinning]" link

Scenario: 3 goodpath
Given I navigate to "https://www.westfield.com/"
And I click on "[Location]" link
And I click on "[Stores]" link
And I click on "[AllStores]" link
And I click on "AllOffers" link
And I capture screenshot

Scenario: 4 goodpath
Given I navigate to "https://www.westfield.com/"
And I click on "[Location]" link
And I click on "[Dinning]" link
And I click on "[AllDinning]" link

